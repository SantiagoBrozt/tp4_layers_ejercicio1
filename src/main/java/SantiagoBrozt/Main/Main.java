package SantiagoBrozt.Main;

import SantiagoBrozt.BaseDeDatos.ParticipanteJdbc;
import SantiagoBrozt.InterfazDeUsuario.AgregarParticipante;
import SantiagoBrozt.Modelo.Participante;
import SantiagoBrozt.Modelo.SistemaDeInscripcion;

import java.awt.EventQueue;
import java.sql.SQLException;
public class Main {

    public static final String URL = "jdbc:mysql://localhost:3306/tp4_ejercicio1";
    public static final String USUARIO = "SantiagoBrozt";
    public static final String COTRASENIA = "okQFuK8ohJKeeeiK";

    public static void main(String[] args) {
        new AgregarParticipante(new SistemaDeInscripcion(new ParticipanteJdbc(URL, USUARIO, COTRASENIA))).iniciar();
    }
}