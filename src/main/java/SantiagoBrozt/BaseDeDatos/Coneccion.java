package SantiagoBrozt.BaseDeDatos;

import java.sql.Connection;
import java.sql.DriverManager;

public class Coneccion {
    private String url;
    private final String usuario;
    private final String contrasenia;
    private Connection coneccion;

    public Coneccion(String url, String usuario, String contrasenia) {
        this.url = url;
        this.usuario = usuario;
        this.contrasenia = contrasenia;
    }

    public Connection abrir() throws RuntimeException {
        try {
            return DriverManager.getConnection(url, usuario, contrasenia);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
