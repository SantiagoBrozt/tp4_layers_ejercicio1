package SantiagoBrozt.BaseDeDatos;

import SantiagoBrozt.Modelo.RegistroDeParticipante;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ParticipanteJdbc implements RegistroDeParticipante {
    private Coneccion coneccion;

    public ParticipanteJdbc(String url, String usuario, String contrasenia) throws RuntimeException {
        this.coneccion = new Coneccion(url, usuario, contrasenia);
    }

    @Override
    public void registrar(String nombre, String telefono, String region) throws RuntimeException {
        String consulta = "INSERT into participante(nombre, telefono, region) values(?,?,?)";
        try (Connection coneccionAbierta = this.coneccion.abrir();
             PreparedStatement st = coneccionAbierta.prepareStatement(consulta)) {
            st.setString(1, nombre);
            st.setString(2, telefono);
            st.setString(3, region);
            st.executeUpdate();
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
