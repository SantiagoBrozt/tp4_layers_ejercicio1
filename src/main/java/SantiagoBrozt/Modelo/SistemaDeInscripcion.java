package SantiagoBrozt.Modelo;

public class SistemaDeInscripcion implements Inscripcion{
    private RegistroDeParticipante registro;

    public SistemaDeInscripcion(RegistroDeParticipante registro) {
        this.registro = registro;
    }

    @Override
    public void inscribir(String nombre, String telefono, String region) throws RuntimeException{
        var p = new Participante(nombre, telefono, region);
        registro.registrar(nombre, telefono, region);
    }
}
