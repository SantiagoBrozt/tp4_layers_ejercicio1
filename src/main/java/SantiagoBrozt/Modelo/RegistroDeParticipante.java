package SantiagoBrozt.Modelo;

public interface RegistroDeParticipante {
    public void registrar(String nombre, String telefono, String region) throws RuntimeException;
}
