package SantiagoBrozt.Modelo;

//import SantiagoBrozt.BaseDeDatos.ParticipanteJdbc.RegistroDeParticipante;

import SantiagoBrozt.BaseDeDatos.ParticipanteJdbc;

public class Participante {
    private String nombre;
    private String telefono;
    private String region;

    public Participante(String nombre, String telefono, String region) throws RuntimeException{
        validarDatos(nombre, telefono, region);
        this.nombre = nombre;
        this.telefono = telefono;
        this.region = region;
    }

    public void validarDatos(String nombre, String telefono, String region) throws RuntimeException {
        if (nombre.equals("")) {
            throw new RuntimeException("debe cargar un nombre");
        }
        if (telefono.equals("")) {
            throw new RuntimeException("debe cargar un telefono");
        }
        if (!validarTelefono(telefono)) {
            throw new RuntimeException("El teléfono debe ingresarse de la siguiente " +
                    "forma: NNNN-NNNNNN");
        }
        if (region.equals("")) {
            throw new RuntimeException("debe cargar una region");
        }
        if (!region.equals("China") && !region.equals("US") && !
                region.equals("Europa")) {
            throw new RuntimeException("Region desconocida. Las conocidas son: " +
                    "China, US, Europa");
        }
    }

    private boolean validarTelefono(String telefono) {
        String regex = "\\d{4}-\\d{6}";
        return telefono.matches(regex);
    }
}
